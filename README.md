# README #
In this repository a data analytics exercise is maintained. The purpose of the exercise is 
the integration of various online data sources with demographic information and disability service providers of New York State 
and the elicitation of various conclusions. 


### Repository Structure: ###
* **Python source code** under directory **./python/** contains all the scripts for the ETL processes, the analysis of the 
data sources and generation of the included visualizations. [Code Dir](https://bitbucket.org/theodori/minerva/src/ec651be2bc34fb27aa794e194636f262cfb13a4b/python/?at=master) 
* A **report** with the description of the followed ETL architecture, the analytical tasks and the various conclusions is 
included at directory **./report** [Report Link](https://bitbucket.org/theodori/minerva/src/ba5b18bb7c9b44d56e1c4ae9c25388fe132da21a/report/REPORT.md?at=master&fileviewer=file-view-default)


### Dependencies and execution: ###

* Python3 
* package requirements as described here:  ./python/requirements.txt
* Execution of the main program:
    * cd python 
    * pip install -r requirements.txt
    * python3 main.py
 

 


 

 
