from etl.analytics import *
from etl.etl import *
from view.app_visualizations import *

print('ETL starting:')
datasources = {}
datasources['nyc_statistics']=etl_nyc_statistics()
datasources['nys_zip']=etl_nys_zip()
datasources['disability_directory']=etl_disability_services()
datasources['merged']=pd.merge(datasources['nyc_statistics'], datasources['nys_zip'])
print('ETL finished------\n')

print('Aggregations:')
datasources['county_demographics']=split_aggregate_on_county(datasources['merged'])
print(datasources['county_demographics'].info())
datasources['county_disability']=disability_aggregate_on_county(datasources['disability_directory'])
print(datasources['county_disability'].info())
print('Aggregations finished------\n')

print(detect_duplicate_relation(datasources['merged']))

nyc_data=datasources['nyc_statistics']
nys_zip=datasources['nys_zip']
disability=datasources['county_disability']
county_data=datasources['county_demographics']

gender_slice=nyc_data[['ZIP Code','percent_gender_total','percent_gender_unknown','percent_male','percent_female','count_participants','count_gender_total']]
visualize_participant_breakdown(gender_slice)
visualize_gender_coverage(gender_slice)
visualize_gender_breakdown(gender_slice)
visualize_participant_sd(nyc_data)
visualize_ethnic_demographics(nyc_data)

visualize_public_assistance_coverage(nyc_data[['ZIP Code','percent_public_assistance_total']])
visualize_public_assistance(nyc_data)
visualize_public_assistance_county(county_data)
visualize_public_assistance_county(county_data)
visualize_public_assistance_county_multi(county_data)
visualize_services_breakdown_population_county(disability,county_data)
print_data_for_map_participation(county_data)
print_data_for_map_services(disability)