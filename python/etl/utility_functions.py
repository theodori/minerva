import urllib.request
import os
from urllib.error import URLError


def download_resource(
        url,
        file_name
):
    """
    Function that downloads the resource and saves it at filesystem @ ./data download dir
    and returns the contents of the resource:

    Attributes:
        url: The url of the online resource.
        file: The filename where the resource should be saved.
    """
    new_file_name=os.getcwd()+'/data/' + file_name
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'}

    os.makedirs(os.path.dirname(new_file_name), exist_ok=True)

    req = urllib.request.Request(url,headers=headers)
    content=None
    try:
        with urllib.request.urlopen(req) as response:
            content = response.read()
            with open(new_file_name, "w+") as f:
                f.write(content.decode(("utf-8")))
    except URLError as e:
        print(e.reason)
        print(e.args)
        content=None
    return content
