import time
import xml.etree.ElementTree as ET

import pandas as pd

from etl.utility_functions import download_resource


class NewYorkDemographicStatisticsResource:
    """
    Representations of resource City of New York Demographic Statistics By Zip Code  :
    The information included in this datasource is: by jurisdiction_name (=Zip Code) include counts and % of the following
      - Gender (Male, Female, Unknown)
      - Ethnic Background: (e.g.
      - Citizen Status:
    """
    url = 'http://data.cityofnewyork.us/api/views/kku6-nxdu/rows.xml?accessType=DOWNLOAD'
    file_name = 'new_york_demographic_stats__'

    def __init__(self):
        date = time.strftime("%Y_%m_%d")
        self.data = pd.DataFrame()
        self.content = None
        self.resource_filename = NewYorkDemographicStatisticsResource.file_name + date + '.xml'
        self.content = download_resource(NewYorkDemographicStatisticsResource.url, self.resource_filename)


    def print_extraction_information(self):
        print('-------------------')
        print('NYC demographic statistics datasource :: Extraction Information: ')
        print(' URL:' + NewYorkDemographicStatisticsResource.url)
        print(' FILENAME:' + self.resource_filename)
        print(' EXTRACTION STATUS:' + str(self.get_extraction_status()))
        print('-------------------')

    def get_extraction_status(self):
        if self.content == None:
            return False
        else:
            return True

    def get_datasource_content(self):
        return self.content

    def transform(self):
        self.data = pd.DataFrame()
        try:
            # Transfrom XML structure to Pandas Dataframe
            if (self.get_extraction_status() == False):
                return None
            root = ET.XML(self.content)  # root element
            for i, subroot in enumerate(root):  # sub-root element as xml starts with <response><row>
                all_rows = []
                for j, child in enumerate(subroot):
                    row = {}
                    for subchild in child:
                        #if (not str(subchild.tag).startswith('count')):  # keep only % aggregates
                            if (not str(subchild.tag).startswith('jurisdiction_name')):  # type casting
                                row[subchild.tag] = float(subchild.text)
                            else:
                                row[subchild.tag] = str(subchild.text)
                    all_rows.append(row)
                self.data = pd.DataFrame(all_rows)
                self.data = self.data.rename(columns={'jurisdiction_name': 'ZIP Code'})
        except Exception as e:
            print(e.args)
            self.data = pd.DataFrame()
        return self.data

    def get_transformation_status(self):
        if self.data.empty:
            return False
        else:
            return True


    def get_datasource_transformed_content(self):
        return self.data

    def print_dataset_information(self):
        print('-------------------')
        print('NYC demographic statistics datasource :: Transformation Information: ')
        print(' TRANSFORMATION STATUS:' + str(self.get_transformation_status()))
        if (self.get_transformation_status()):
            print(self.data.info())
        print('-------------------')
