import pandas as pd

def split_aggregate_on_county(
        data
):
    """
    Function that extracts the counts and aggregates based on County

    """
    merged = data
    attributes = list(merged.columns.values)
    count_iter = filter(lambda x: x.startswith('count'), attributes)
    count_list = ['County Code','County Name']
    for x in count_iter:
        count_list.append(x)
    count_data=data[count_list]
    county_counts=count_data.groupby(['County Code','County Name']).sum()
    county_counts.reset_index(level=1, inplace=True)
    county_counts.reset_index(level=0, inplace=True)
    return county_counts

def disability_aggregate_on_county(
        data
):
    """
    Function that extracts counts on produced services and aggregates based on County

    """

    cols = ['created_at','created_meta', 'position', 'sid', 'updated_at']
    split=data
    zip_county_relation=data[['Zip Code', 'County Name']]
    split.drop(cols, inplace=True, axis=1)
    zip_group = split.groupby(['Zip Code']).sum() # Aggregate per zip level
    zip_group.reset_index(level=0, inplace=True)
    merge_back=pd.merge(zip_county_relation,zip_group,how='left')
    merge_back = merge_back.groupby(['County Name']).sum() # Aggregate at County level
    #merge_back=merge_back.groupby('County Name').agg(['sum', 'count'])
    merge_back.reset_index(level=0, inplace=True)
    merge_back['Total Services']=merge_back[merge_back.columns.values].sum(axis=1)

    zip_group2 = split.groupby(['Zip Code']).size()  # Aggregate per zip level
    zip_group2 =pd.DataFrame(zip_group2)
    zip_group2 = zip_group2.rename(columns={0: 'Total Service Agents'})
    zip_group2.reset_index(level=0, inplace=True)
    merge_back2 = pd.merge(zip_county_relation, zip_group2, how='left')
    merge_back2 = merge_back2.groupby('County Name').sum()  # Aggregate at County level
    merge_back2.reset_index(level=0, inplace=True)
    merge_back = pd.merge(merge_back, merge_back2, how='left')
    return merge_back

def detect_duplicate_relation(
        merged
):
    county_zip = merged[['County Name', 'ZIP Code']]
    counted=county_zip.groupby(['ZIP Code']).count()
    counted = counted.rename(columns={'County Name': 'County Name Count'})
    counted['ZIP Code'] = counted.index
    dublicate=counted[counted['County Name Count'] > 1]
    merge_back=pd.merge(dublicate,county_zip,how='left')
    return merge_back[['ZIP Code', 'County Name']]



