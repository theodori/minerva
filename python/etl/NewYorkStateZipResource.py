import time
from io import StringIO

import pandas as pd

from etl.utility_functions import download_resource


class NewYorkStateZipResource:
    """
    Representations of resource State of New York Demographic Zip Code Information  :
    It is a relation including the following attributes:
    County Name	State FIPS	County Code	County FIPS	ZIP Code
    """
    url = 'http://data.ny.gov/api/views/juva-r6g2/rows.csv?accessType=DOWNLOAD'
    file_name = 'new_york_state_zip__'

    def __init__(self):
        date = time.strftime("%Y_%m_%d")
        self.data = pd.DataFrame()
        self.content = None
        self.resource_filename = NewYorkStateZipResource.file_name + date + '.csv'
        self.content = download_resource(NewYorkStateZipResource.url, self.resource_filename)


    def print_extraction_information(self):
        print('-------------------')
        print('NYC Zip Code datasource :: Extraction Information: ')
        print(' URL:' + NewYorkStateZipResource.url)
        print(' FILENAME:' + self.resource_filename)
        print(' EXTRACTION STATUS:' + str(self.get_extraction_status()))
        print('-------------------')

    def get_extraction_status(self):
        if self.content == None:
            return False
        else:
            return True

    def get_datasource_content(self):
        return self.content

    def transform(self):
        self.data = pd.DataFrame()
        try:
            self.data=pd.read_csv(StringIO(self.content.decode(("utf-8"))),index_col=None)
            self.data['ZIP Code'] = self.data['ZIP Code'].astype(str)
        except Exception as e:
            print(e.args)
            self.data = pd.DataFrame()
        return self.data

    def get_transformation_status(self):
        if self.data.empty:
            return False
        else:
            return True


    def get_datasource_transformed_content(self):
        return self.data

    def print_dataset_information(self):
        print('-------------------')
        print('NYS zip info datasource :: Transformation Information: ')
        print(' TRANSFORMATION STATUS:' + str(self.get_transformation_status()))
        if (self.get_transformation_status()):
            print(self.data.info())
        print('-------------------')
