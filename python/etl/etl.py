import pandas as pd

from etl.DisabilityServicesDirectoryResource import DisabilityServicesDirectoryResource
from etl.NewYorkDemographicStatisticsResource import NewYorkDemographicStatisticsResource
from etl.NewYorkStateZipResource import NewYorkStateZipResource


def etl_nyc_statistics():
    print('Datasource: NYC demographic statistics:')

    print('Retrieve and print information:')
    new_york_demographic_statistics=NewYorkDemographicStatisticsResource()
    new_york_demographic_statistics.print_extraction_information()

    if (new_york_demographic_statistics.get_extraction_status()==False):
        print('Retrieval Failed')
        exit(1)

    new_york_demographic_statistics.transform()

    if (new_york_demographic_statistics.get_transformation_status()==False):
        print('Tranformation Failed')
        exit(1)

    new_york_demographic_statistics.print_dataset_information()
    return new_york_demographic_statistics.get_datasource_transformed_content()

def etl_nys_zip():
    print('Datasource: NY State Zip Info:')

    print('Retrieve and print information:')
    new_work_zip=NewYorkStateZipResource()
    new_work_zip.print_extraction_information()

    if (new_work_zip.get_extraction_status()==False):
        print('Retrieval Failed')
        exit(1)

    new_work_zip.transform()

    if (new_work_zip.get_transformation_status()==False):
        print('Tranformation Failed')
        exit(1)

    new_work_zip.print_dataset_information()
    return new_work_zip.get_datasource_transformed_content()

def etl_disability_services():
    print('Datasource: Disability Services Directory:')

    print('Retrieve and print information:')
    datasource=DisabilityServicesDirectoryResource()
    datasource.print_extraction_information()

    if (datasource.get_extraction_status()==False):
        print('Retrieval Failed')
        exit(1)

    datasource.transform()

    if (datasource.get_transformation_status()==False):
        print('Tranformation Failed')
        exit(1)

    datasource.print_dataset_information()
    return datasource.get_datasource_transformed_content()



