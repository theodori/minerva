import json
import time
import traceback

import pandas as pd

from etl.utility_functions import download_resource


class DisabilityServicesDirectoryResource:
    """
    Representations of disability services directory  :
    Per Zip Code and County provide the Disability Services and the services provided
    """
    url = 'http://data.ny.gov/api/views/ieqx-cqyk/rows.json?accessType=DOWNLOAD'
    file_name = 'disabilities_services_directory__'

    def __init__(self):
        date = time.strftime("%Y_%m_%d")
        self.data = pd.DataFrame()
        self.content = None
        self.resource_filename = DisabilityServicesDirectoryResource.file_name + date + '.json'
        self.content = download_resource(DisabilityServicesDirectoryResource.url, self.resource_filename)


    def print_extraction_information(self):
        print('-------------------')
        print('Disabilities Services Directory :: Extraction Information: ')
        print(' URL:' + DisabilityServicesDirectoryResource.url)
        print(' FILENAME:' + self.resource_filename)
        print(' EXTRACTION STATUS:' + str(self.get_extraction_status()))
        print('-------------------')

    def get_extraction_status(self):
        if self.content == None:
            return False
        else:
            return True

    def get_datasource_content(self):
        return self.content

    def transform(self):
        self.data = pd.DataFrame()
        try:
            json_content= json.loads(self.content.decode(("utf-8")))
            cols=[]
            for col in json_content['meta']['view']['columns']: # load column names
                cols.append(col['name'])
            all_rows = []
            for record in json_content['data']: # load records and match attributes with the corresponding column names
                row = {}
                i=0
                for key in record:
                    col_name=cols[i]
                    i=i+1
                    if key == 'Y':
                        row[col_name] =  1
                    elif key == 'N':
                        row[col_name] =  0
                    else:
                        row[col_name] =  key
                all_rows.append(row)
            self.data = pd.DataFrame(all_rows)
            self.data = self.data.rename(columns={'County': 'County Name'})
            self.data['County Name']=self.data['County Name'].str.capitalize()
        except Exception as e:
            print(e.args)
            traceback.print_tb(e.__traceback__)
            self.data = pd.DataFrame()
        return self.data

    def get_transformation_status(self):
        if self.data.empty:
            return False
        else:
            return True


    def get_datasource_transformed_content(self):
        return self.data

    def print_dataset_information(self):
        print('-------------------')
        print('Disability Services Directory datasource :: Transformation Information: ')
        print(' TRANSFORMATION STATUS:' + str(self.get_transformation_status()))
        if (self.get_transformation_status()):
            print(self.data.info())
        print('-------------------')
