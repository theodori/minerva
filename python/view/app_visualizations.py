import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import parallel_coordinates

# Participant and gender visualizations
def visualize_gender_coverage(data: pd.DataFrame):
    plt.style.use('ggplot')
    total = data['percent_gender_total'].count()
    records_with_gender = data['percent_gender_total'][data['percent_gender_total'] == 100].count()
    records_with_no_gender = data['percent_gender_total'][data['percent_gender_total'] == 0].count()
    t = [records_with_gender / total, records_with_no_gender / total]
    series = pd.Series(t, index=['with gender info', 'without gender info'], name='Gender Coverage (% of areas)')
    series.plot.pie(figsize=(6, 6))
    plt.show()


def visualize_participant_breakdown(data: pd.DataFrame):
    plt.style.use('ggplot')
    data = data[['ZIP Code', 'count_participants']]
    sorted_data = data[data['count_participants'] > 0].sort_values(['count_participants'])
    sorted_data.plot.bar()
    plt.show()


def visualize_participant_sd(data: pd.DataFrame):
    plt.style.use('ggplot')
    color = dict(boxes='DarkGreen', whiskers='DarkOrange', medians='DarkBlue', caps='Gray')
    data = data[['count_participants', 'count_male', 'count_female']][data['count_participants'] > 0]
    data.plot.box(color, sym='r+',vert=False)
    plt.show()

def visualize_ethnic_demographics(data: pd.DataFrame):
    plt.style.use('ggplot')
    data = data[data['count_participants'] > 0]
    data = data[['percent_black_non_hispanic', 'percent_asian_non_hispanic', 'percent_hispanic_latino',
                 'percent_white_non_hispanic']].sort_values(['percent_white_non_hispanic'])
    data.plot.bar(stacked=True)
    plt.show()

def visualize_public_assistance(data: pd.DataFrame):
    plt.style.use('ggplot')
    data1 = data[data['count_participants'] > 0].sort_values(['percent_white_non_hispanic'])
    data2 = data1[['percent_black_non_hispanic', 'percent_asian_non_hispanic', 'percent_hispanic_latino',
                 'percent_white_non_hispanic']]
    data3 = data1['count_receives_public_assistance']
    data2.plot.bar(stacked=True)
    data3.plot.bar(secondary_y=True,  color='#000000')
    plt.show()

def visualize_public_assistance_county(data: pd.DataFrame):
    plt.style.use('ggplot')
    data1 = data[data['count_participants'] > 0].sort_values(['count_white_non_hispanic'])
    data1.set_index('County Name', inplace=True)
    data2 = data1[['count_black_non_hispanic', 'count_asian_non_hispanic', 'count_hispanic_latino',
                 'count_white_non_hispanic']]
    data3 = data1['count_receives_public_assistance']
    data2.plot.bar(stacked=True)
    data3.plot.bar(color='#000000', width=0.1)
    plt.show()

def visualize_public_assistance_county_multi(data: pd.DataFrame):
    plt.style.use('ggplot')
    data1 = data[data['count_participants'] > 0].sort_values(['count_white_non_hispanic'])
    data2 = data1[['County Name','count_black_non_hispanic', 'count_asian_non_hispanic', 'count_hispanic_latino',
                 'count_white_non_hispanic']]
    print(data2.info())
    parallel_coordinates(data2, 'County Name')
    plt.show()

def visualize_gender_breakdown(data: pd.DataFrame):
    plt.style.use('ggplot')
    data = data[['percent_male', 'percent_female', 'count_participants']]
    sorted_data = data[data['count_participants'] > 0].sort_values(['percent_male'] + ['percent_female'])
    sorted_data[['percent_male', 'percent_female']].plot.bar(stacked=True)
    plt.show()

# Public assistance visualizations
def visualize_public_assistance_coverage(data: pd.DataFrame):
    plt.style.use('ggplot')
    total = data['percent_public_assistance_total'].count()
    records_with_pa = data['percent_public_assistance_total'][data['percent_public_assistance_total'] == 100].count()
    records_with_no_pa = data['percent_public_assistance_total'][data['percent_public_assistance_total'] == 0].count()
    t = [records_with_pa / total, records_with_no_pa / total]
    series = pd.Series(t, index=['with public assistance info', 'without public assistance info'],
                       name='Public Assistance (% of areas)')
    series.plot.pie(figsize=(6, 6))
    plt.show()


def visualize_public_assistance_breakdown(data: pd.DataFrame):
    plt.style.use('ggplot')
    data.plot.scatter(x='percent_black_non_hispanic', y='percent_white_non_hispanic', c='count_public_assistance_total')
    plt.show()


def visualize_gender_breakdown_county(data: pd.DataFrame):
    plt.style.use('ggplot')
    data = data[['count_male', 'count_female', 'count_participants']]
    sorted_data = data.sort_values(['count_male'] + ['count_female'])
    sorted_data.plot.bar(stacked=True)
    plt.show()

def visualize_participant_breakdown_county(data: pd.DataFrame):
    plt.style.use('ggplot')
    county_Data=data
    county_Data.set_index('County Name',inplace=True)
    county_Data = county_Data[[ 'count_participants']]
    sorted_data = county_Data[county_Data['count_participants'] > 0].sort_values(['count_participants'])
    sorted_data.plot.bar()
    plt.show()

def visualize_services_breakdown_county(data: pd.DataFrame):
    plt.style.use('ggplot')
    print(data.head(1))
    county_Data = data
    county_Data.set_index('County Name', inplace=True)
    county_Data = county_Data['Total Services']
    county_Data.plot.bar()
    plt.show()

def visualize_services_breakdown_population_county(services: pd.DataFrame, demographics: pd.DataFrame):
    plt.style.use('ggplot')
    county_services = services
    county_Data = demographics
    print(county_services.info())
    print(county_Data.info())
    county_Data = pd.merge(county_Data, county_services, how='left')
    sorted_data = county_Data[county_Data['count_participants'] > 0].sort_values(['count_participants'])
    sorted_data=sorted_data.set_index('County Name')
    sorted_data=sorted_data[['Total Services', 'Total Service Agents', 'count_participants']]
    sp_per_person=sorted_data

    sp_per_person.plot.scatter(x='Total Service Agents', c='count_participants',y='Total Services' )
    plt.show()

def print_data_for_map_participation(data: pd.DataFrame):
    county_Data = data[data['count_participants'] > 0]
    county_Data.set_index('County Name', inplace=True)
    max = county_Data['count_participants'].max()
    county_Data['normalized_participants']=county_Data['count_participants']/max
    r=county_Data['normalized_participants']
    print(r)
    print(r.to_json())

def print_data_for_map_services(services: pd.DataFrame):
    county_services = services
    county_services=county_services.set_index('County Name', inplace=False)
    county_services['Total Service Agents']=county_services['Total Service Agents']/500
    print(county_services['Total Service Agents'])
    print(county_services['Total Service Agents'].to_json())