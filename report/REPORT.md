# Introduction #
New York State (https://catalog.data.gov/organization/state-of-new-york) and New York City 
(https://catalog.data.gov/dataset?publisher=data.cityofnewyork.us) are maintaining a number of public 
datasets including useful information across several sectors and topics like: demographic census data, education, 
traffic, environmental, financial activities, crime statistics etc.  The purpose of this exercise is to investigate 
if the number of the provided developmental disabilities service provides across the various NYS countries is correlated 
with the various demographic characteristics of the local population. A number of data sources was integrated, normalized, 
and analysed and a number of relevant visulization was generated and presented in the forthcoming sections.
 
The datasets under consideration are :

1.	**Data source 1:** Demographic statistics broken down by zip code (https://catalog.data.gov/dataset/demographic-statistics-by-zip-code-acfc9). 
The file type of this datasource is XML and contains various demographic information like number of participants, gender, 
ethnic group, citizenship and receiving public assistance breakdown by zip code.

2.	**Data source 2:** Directory of Developmental Disabilities Service Provider Agencies (https://catalog.data.gov/dataset/directory-of-developmental-disabilities-service-provider-agencies). 
This is a directory of the developmental disabilities service providers including the type of the provided services and 
the address or each provider. The file type of this data source is CSV.

3. **Data source 3:** New York State ZIP Codes-County FIPS Cross-Reference (https://catalog.data.gov/dataset/new-york-state-zip-codes-county-fips-cross-reference).
A listing of NYS counties with accompanying US Postal Service ZIP codes. The file type of this data source is JSON and 
it is required in order to process and aggregate information of the aforementioned datasets at county level. 

# ETL and Analytic Processes Architecture #
The overall structure of the ETL and the analytic processes is highlighted in the following diagram.
 
![structure](structure.png)

* **ETL:** (source under python/etl/etl.py) The ETL is maintaining for each one of the data sources a separate process 
(DisabilityServicesDirectoryResource.py, NewYorkDemographicStatisticsResource.py, NewYorkStateZipResource.py).
For each one of the data sources, a specific set of metadata is maintained (like last access date, URL, local filename, 
tha status -Success/Failure- of each one of the steps etc.) and a common set of steps is performed:
    * **Extract:** The data source is fetched from the online resource, it is stored in local data repository 
    (directory ./data/ ) and the required set of metadata is populated. 
    * **Transform:** Then each one of the data sources is parsed and transformed as required for the forthcoming stages. Briefly, from
      each one of the incoming data structures (XML, CSV, JSON) information is transformed in dataframes (relational model).
       During this transformation a number of changes in attribute names, data types is performed in order next stages to 
       have a common point of reference. 
    * **Load:** Finally, the transformed datasets are maintained in a data structure (collection) of dataframes.
           
* **Analytics** (source under python/etl/analytics.py) After the ETL process a number of analytical tasks is executed 
in order to merge, join and analyze the input datasets. Data source 1 and data source 2 (containing information at zip level) 
are joined and various aggregate statistics are calculated at county level with the use of data source 3 (that contained 
the zip to county relation).  

* **Apps/Visualizations** (source under python/view/app_visualizations.py). The aggregated datasets are consumed by various
visualization apps that facilitate the analysis of the data sources.

# Analysis #

For the analysis of the data sources the following questions were under consideration:

##	1. At zip code level investigate the male and female division:

As we can see in the following diagrams the dataset seem partial. The participation in the census was not at the same level
 in each one of the zip code areas. Possibly because of variation in the population density or at the participation levels
 of the survey.

![participant_distribution](participant_distribution.png)

Moreover, less than 50% of the zip areas include gender information:

![gender_coverage](gender_coverage.png)

Finally, the female participants seem that are slightly more that the male. +10 on average: 

![gender_distribution](gender_distribution.png)
![gender_participant](gender_participant_sd.png)

## 2.	Investigate a breakdown of the various demographics at zip code level:

![ethnic_breakdown](ethnic_breakdown.png)

As we can see from the above breakdown of the ethnic groups, it seems that there are quite a few zip code areas with the 
white ethnic background population to be dominant. In the rest of the zip code areas the composition of local population is 
evenly spread across the black and hispanic ethnic background groups.  

## 3.	Investigate a breakdown of the various demographics at county level
  
  The following ethnic breakdown integrated with the public assistance breakdown depicts that the number of 
  people receiving public assistance (dark black bars with scale expressed with the right side y-axis)
   is bigger at zip code areas with white population. 
   
  ![ethnic_public_assistance_breakdown](ethnic_public_assistance_breakdown.png)
  
  By aggregating information at county level we can observe the same relation.

  ![ethnic_public_assistance_county](ethnic_public_assistance_county.png)
  
  (Note that the observed correlation might be affected by other variables like the location of the county (central/remote) 
   or the number of the public services at this location etc.)
  
## 4. Investigate skews of the various demographics at  county level

As we can observe from the following diagram counties like 'Sullivan', 'Kings' and 'Ulster' differentiate from the rest of
 the counties as they have the largest number of participants with white ethnic background. Similarly, 'Bronx' has a large 
  number of participants but evenly composed with citizens with black and hispanic ethnic background. The rest 
   of the counties seem to have similar setup with respect to the number of participants and ethnic background breakdown. 
  
![ethnic_county_breakdown](ethnic_county_breakdown.png)
  
## 5. Are there counties which are disadvantaged when it comes to access to Developmental Disabilities Service Providers?

As it is depicted in the following two diagrams. There are counties with quite small number of provided services with respect 
to the size of their local population. For example 'Sullivan', 'Bronx' etc.

   ![total_services_vs_participants.png](total_services_vs_participants.png)

   This is also depicted in the following figure. Left-bottom dark points are counties with 
   * 'large population' and 
   * the smallest number of Developmental Disabilities Service Providers  and 
   * the smallest number total number of provided services (as each provider can support different types of disabilities 
   this variable is also useful in order to measure the coverage of the population).
   
   Note that the diagram y-axis depicts the total number of services in the area and x-axis expresses the average number 
   of service provider per participant.

   ![total_services_vs_participants2.png](total_services_vs_participants_2.png)

## Notes 

* A small number of zip code areas (9) it seems that belongs to more than one counties (see table bellow). A further 
  improvement might be either to distribute evenly the number of observations of each one of these zip code areas 
  to the associated counties or distribute proportionally to the total number of participants based on the rest of the areas inside
    each county.

```
   ZIP Code  County Name
0     10463        Bronx
1     10463     New York
2     10705  Westchester
3     10705        Bronx
4     11001       Nassau
5     11001       Queens
6     11201        Kings
7     11201     New York
8     11239        Kings
9     11239       Queens
10    11580       Nassau
11    11580       Queens
12    11693       Queens
13    11693        Kings
14    12428       Ulster
15    12428     Sullivan
16    12458       Ulster
17    12458     Sullivan
18    12758     Sullivan
19    12758       Ulster
20    12758     Delaware
21    12763     Sullivan
22    12763       Ulster
```

* As the demographic data are sparse (e.g. there are zip codes with no participants or observations) a further improvement
might be interpolate the observations of these areas based on observations of the adjacent areas (neighborhood zip codes).

The following image depicts the participant counts (green areas have smaller number of participants while red express 
bigger participation numbers)

![map.png](map.png)

The following map depicts the number of service provider per county. Grey areas have no information at all.
![map3.png](map3.png)
